<img height="60px" width="60px" style="float: left;" src="/assets/img/demo/logo.svg" alt="Vex Logo">
<h2 style="height: 60px; line-height: 60px; margin-left: 70px; font-weight: 500; border: none;">VEX</h2>

# Changelog

## 1.0.0 (2019-08-22)

### Features
- Initial release
