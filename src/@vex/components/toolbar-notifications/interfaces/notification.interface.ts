import { DateTime } from 'luxon';
import { CSSVariable } from '../../../interfaces/css-variable.enum';
import { Icon } from '@visurel/iconify-angular';

export interface Notification {
  id: string;
  icon: Icon;
  label: string;
  color: CSSVariable;
  datetime: DateTime;
  read?: boolean;
}
