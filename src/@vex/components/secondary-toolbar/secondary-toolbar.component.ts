import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'vex-secondary-toolbar',
  templateUrl: './secondary-toolbar.component.html',
  styleUrls: ['./secondary-toolbar.component.scss']
})
export class SecondaryToolbarComponent implements OnInit {

  @Input() current: string;
  @Input() crumbs: string[];

  constructor() { }

  ngOnInit() {
  }
}
