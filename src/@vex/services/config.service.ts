import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { DOCUMENT } from '@angular/common';
import { DeepPartial } from '../interfaces/deep-partial.type';
import { mergeDeep } from '../utils/merge-deep';

export interface ConfigProps {
  id: ConfigName;
  rtl?: boolean;
  layout: 'vertical' | 'horizontal';
  boxed: boolean;
  sidenav: {
    layout: 'expanded' | 'collapsed';
  };
  toolbar: {
    fixed: boolean;
  };
  footer: {
    visible: boolean;
    fixed: boolean;
  };
}

export enum ConfigName {
  default = 'vex-default',
  dark = 'vex-dark',
  light = 'vex-light',
  verticalDefault = 'vex-vertical-default',
  verticalDark = 'vex-vertical-dark',
  verticalLight = 'vex-vertical-light'
}

export type Config = {
  [key in ConfigName]: ConfigProps;
};

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  defaultConfig = ConfigName.default;

  configs: Config = {
    [ConfigName.default]: {
      id: ConfigName.default,
      layout: 'horizontal',
      boxed: false,
      sidenav: {
        layout: 'expanded'
      },
      toolbar: {
        fixed: true
      },
      footer: {
        visible: true,
        fixed: true
      }
    },
    [ConfigName.dark]: {
      id: ConfigName.dark,
      layout: 'horizontal',
      boxed: false,
      sidenav: {
        layout: 'expanded'
      },
      toolbar: {
        fixed: true
      },
      footer: {
        visible: true,
        fixed: true
      }
    },
    [ConfigName.light]: {
      id: ConfigName.light,
      layout: 'horizontal',
      boxed: false,
      sidenav: {
        layout: 'expanded'
      },
      toolbar: {
        fixed: true
      },
      footer: {
        visible: true,
        fixed: true
      }
    },
    [ConfigName.verticalDefault]: {
      id: ConfigName.verticalDefault,
      layout: 'vertical',
      boxed: true,
      sidenav: {
        layout: 'expanded'
      },
      toolbar: {
        fixed: false
      },
      footer: {
        visible: true,
        fixed: true
      }
    },
    [ConfigName.verticalDark]: {
      id: ConfigName.verticalDark,
      layout: 'vertical',
      boxed: true,
      sidenav: {
        layout: 'expanded'
      },
      toolbar: {
        fixed: false
      },
      footer: {
        visible: true,
        fixed: true
      }
    },
    [ConfigName.verticalLight]: {
      id: ConfigName.verticalLight,
      layout: 'vertical',
      boxed: true,
      sidenav: {
        layout: 'expanded'
      },
      toolbar: {
        fixed: false
      },
      footer: {
        visible: true,
        fixed: true
      }
    },
  };

  private _configSubject = new BehaviorSubject(this.configs[this.defaultConfig]);
  config$ = this._configSubject.asObservable();

  constructor(@Inject(DOCUMENT) private document: Document) {
    this.config$.subscribe(config => this._updateTheme(config));
  }

  setConfig(config: ConfigName) {
    if (this.configs[config]) {
      this._configSubject.next(this.configs[config]);
    }
  }

  update(config: DeepPartial<ConfigProps>) {
    this._configSubject.next(mergeDeep({ ...this._configSubject.getValue() }, config));
  }

  private _updateTheme(config: ConfigProps) {
    const body = this.document.body;
    Object.keys(this.configs).forEach(key => {
      if (body.classList.contains(key)) {
        body.classList.remove(key);
      }
    });

    body.classList.add(config.id);

    // Workaround so charts and other externals know they have to resize on Layout switch
    if (window) {
      window.dispatchEvent(new Event('resize'));

      setTimeout(() => {
        window.dispatchEvent(new Event('resize'));
      }, 200);
    }
  }
}
