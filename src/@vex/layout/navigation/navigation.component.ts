import { Component, OnInit } from '@angular/core';
import { NavigationService } from '../../services/navigation.service';
import { trackByRoute } from '../../utils/track-by';
import { NavigationDropdown, NavigationItem, NavigationLink, NavigationSubheading } from '../../interfaces/navigation-item.interface';
import { NavigationEnd, Router } from '@angular/router';
import { filter, map, startWith } from 'rxjs/operators';

@Component({
  selector: 'vex-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  isActive$ = this.router.events.pipe(
    filter<NavigationEnd>(event => event instanceof NavigationEnd),
    startWith(null),
    map(() => (item: NavigationDropdown) => this.hasActiveChilds(item))
  );

  isLink = this.navigationService.isLink;
  isDropdown = this.navigationService.isDropdown;
  isSubheading = this.navigationService.isSubheading;
  items = this.navigationService.items.reduce<Array<NavigationSubheading & { children: NavigationItem[] }>>((prev, item) => {
    if (this.isSubheading(item)) {
      prev.push({
        ...item as NavigationSubheading,
        children: []
      });
    } else {
      prev[prev.length - 1].children.push(item);
    }

    return prev;
  }, []);
  trackByRoute = trackByRoute;

  constructor(private navigationService: NavigationService,
              private router: Router) { }

  ngOnInit() {
  }

  hasActiveChilds(parent: NavigationDropdown): boolean {
    return parent.children.some(child => {
      if (this.isDropdown(child)) {
        return this.hasActiveChilds(child);
      }

      if (this.isLink(child) && !this.isFunction(child.route)) {
        return this.router.isActive(child.route as string, false);
      }
    });
  }

  isFunction(prop: NavigationLink['route']) {
    return prop instanceof Function;
  }
}
