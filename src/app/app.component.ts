import { Component, Inject, LOCALE_ID, Renderer2 } from '@angular/core';
import { ConfigName, ConfigService } from '../@vex/services/config.service';
import { MatIconRegistry } from '@angular/material/icon';
import { Settings } from 'luxon';
import { DOCUMENT } from '@angular/common';
import { Platform } from '@angular/cdk/platform';
import { NavigationService } from '../@vex/services/navigation.service';
import icLayers from '@iconify/icons-ic/twotone-layers';
import icAssigment from '@iconify/icons-ic/twotone-assignment';
import icContactSupport from '@iconify/icons-ic/twotone-contact-support';
import icDateRange from '@iconify/icons-ic/twotone-date-range';
import icChat from '@iconify/icons-ic/twotone-chat';
import icContacts from '@iconify/icons-ic/twotone-contacts';
import icAssessment from '@iconify/icons-ic/twotone-assessment';
import icLock from '@iconify/icons-ic/twotone-lock';
import icWatchLater from '@iconify/icons-ic/twotone-watch-later';
import icError from '@iconify/icons-ic/twotone-error';
import icAttachMoney from '@iconify/icons-ic/twotone-attach-money';
import icPersonOutline from '@iconify/icons-ic/twotone-person-outline';
import icReceipt from '@iconify/icons-ic/twotone-receipt';
import icHelp from '@iconify/icons-ic/twotone-help';
import icBook from '@iconify/icons-ic/twotone-book';
import icBubbleChart from '@iconify/icons-ic/twotone-bubble-chart';
import icFormatColorText from '@iconify/icons-ic/twotone-format-color-text';
import icStar from '@iconify/icons-ic/twotone-star';
import icViewCompact from '@iconify/icons-ic/twotone-view-compact';
import icPictureInPicture from '@iconify/icons-ic/twotone-picture-in-picture';
import icSettings from '@iconify/icons-ic/twotone-settings';
import { LayoutService } from '../@vex/services/layout.service';
import { CSSVariable } from '../@vex/interfaces/css-variable.enum';
import icUpdate from '@iconify/icons-ic/twotone-update';
import { ActivatedRoute } from '@angular/router';
import { filter } from 'rxjs/operators';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { SplashScreenService } from '../@vex/services/splash-screen.service';

@Component({
  selector: 'vex-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'vex';

  constructor(private configService: ConfigService,
              private iconRegistry: MatIconRegistry,
              private renderer: Renderer2,
              private platform: Platform,
              @Inject(DOCUMENT) private document: Document,
              @Inject(LOCALE_ID) private localeId: string,
              private layoutService: LayoutService,
              private route: ActivatedRoute,
              private navigationService: NavigationService,
              private splashScreenService: SplashScreenService) {
    this.iconRegistry.setDefaultFontSetClass('iconify');
    Settings.defaultLocale = this.localeId;

    if (this.platform.BLINK) {
      this.renderer.addClass(this.document.body, 'is-blink');
    }

    this.route.queryParamMap.pipe(
      filter(queryParamMap => queryParamMap.has('rtl') && coerceBooleanProperty(queryParamMap.get('rtl')))
    ).subscribe(queryParamMap => {
      this.document.body.dir = 'rtl';
      this.configService.update({
        rtl: true
      });
    });

    this.route.queryParamMap.pipe(
      filter(queryParamMap => queryParamMap.has('style'))
    ).subscribe(queryParamMap => {
      this.configService.setConfig(queryParamMap.get('style') as ConfigName);
    });

    this.navigationService.items = [
      {
        label: 'Dashboards',
        onlyVertical: true
      },
      {
        label: 'Dashboard',
        route: '/dashboards/analytics',
        icon: icLayers
      },
      {
        label: 'Apps'
      },
      {
        label: 'All-In-One Table',
        route: '/apps/aio-table',
        icon: icAssigment
      },
      {
        label: 'Help Center',
        icon: icContactSupport,
        children: [
          {
            label: 'Getting Started',
            route: '/apps/help-center/getting-started'
          },
          {
            label: 'Pricing & Plans',
            route: '/apps/help-center/pricing'
          },
          {
            label: 'FAQ',
            route: '/apps/help-center/faq'
          },
          {
            label: 'Guides',
            route: '/apps/help-center/guides'
          }
        ]
      },
      {
        label: 'Calendar',
        route: '/apps/calendar',
        icon: icDateRange,
        badge: {
          value: '12',
          background: CSSVariable['color-deep-purple-500'],
          color: CSSVariable['color-deep-purple-contrast-500']
        },
      },
      {
        label: 'Chat',
        route: '/apps/chat',
        icon: icChat,
        badge: {
          value: '16',
          background: CSSVariable['color-cyan-500'],
          color: CSSVariable['color-cyan-contrast-600']
        },
      },
      {
        label: 'Contacts',
        icon: icContacts,
        children: [
          {
            label: 'List - Grid',
            route: '/apps/contacts/grid',
          },
          {
            label: 'List - Table',
            route: '/apps/contacts/table',
          }
        ]
      },
      {
        label: 'Scrumboard',
        route: '/apps/scrumboard',
        icon: icAssessment,
        badge: {
          value: 'NEW',
          background: CSSVariable['color-primary-500'],
          color: CSSVariable['color-primary-contrast-500']
        }
      },
      {
        label: 'Pages'
      },
      {
        label: 'Authentication',
        icon: icLock,
        children: [
          {
            label: 'Login',
            route: '/login'
          },
          {
            label: 'Register',
            route: '/register'
          },
          {
            label: 'Forgot Password',
            route: '/forgot-password'
          }
        ]
      },
      {
        label: 'Coming Soon',
        icon: icWatchLater,
        route: '/coming-soon'
      },
      {
        label: 'Errors',
        icon: icError,
        badge: {
          value: '4',
          background: CSSVariable['color-green-500'],
          color: CSSVariable['color-green-contrast-600']
        },
        children: [
          {
            label: '404',
            route: '/pages/error-404'
          },
          {
            label: '500',
            route: '/pages/error-500'
          }
        ]
      },
      {
        label: 'Pricing',
        icon: icAttachMoney,
        route: '/pages/pricing'
      },
      {
        label: 'Profile',
        icon: icPersonOutline,
        route: '/pages/profile'
      },
      {
        label: 'Invoice',
        icon: icReceipt,
        route: '/pages/invoice'
      },
      {
        label: 'FAQ',
        icon: icHelp,
        route: '/pages/faq'
      },
      {
        label: 'Guides',
        icon: icBook,
        route: '/pages/guides',
        badge: {
          value: '18',
          background: CSSVariable['color-teal-500'],
          color: CSSVariable['color-teal-contrast-500']
        },
      },
      {
        label: 'UI Elements'
      },
      {
        label: 'Components',
        icon: icBubbleChart,
        children: [
          {
            label: 'Overview',
            route: '/ui/components/overview'
          },
          {
            label: 'Autocomplete',
            route: '/ui/components/autocomplete'
          },
          {
            label: 'Buttons',
            route: '/ui/components/buttons'
          },
          {
            label: 'Button Group',
            route: '/ui/components/button-group'
          },
          {
            label: 'Cards',
            route: '/ui/components/cards'
          },
          {
            label: 'Checkbox',
            route: '/ui/components/checkbox'
          },
          {
            label: 'Dialogs',
            route: '/ui/components/dialogs'
          },
          {
            label: 'Grid List',
            route: '/ui/components/grid-list'
          },
          {
            label: 'Input',
            route: '/ui/components/input'
          },
          {
            label: 'Lists',
            route: '/ui/components/lists'
          },
          {
            label: 'Menu',
            route: '/ui/components/menu'
          },
          {
            label: 'Progress',
            route: '/ui/components/progress'
          },
          {
            label: 'Progress Spinner',
            route: '/ui/components/progress-spinner'
          },
          {
            label: 'Radio',
            route: '/ui/components/radio'
          },
          {
            label: 'Slide Toggle',
            route: '/ui/components/slide-toggle'
          },
          {
            label: 'Slider',
            route: '/ui/components/slider'
          },
          {
            label: 'Snack Bar',
            route: '/ui/components/snack-bar'
          },
          {
            label: 'Tooltip',
            route: '/ui/components/tooltip'
          },
        ]
      },
      {
        label: 'Forms',
        icon: icFormatColorText,
        children: [
          {
            label: 'Form Elements',
            route: '/ui/forms/form-elements'
          },
          {
            label: 'Form Wizard',
            route: '/ui/forms/form-wizard'
          }
        ]
      },
      {
        label: 'Icons',
        icon: icStar,
        children: [
          {
            label: 'Material Icons',
            route: '/ui/icons/ic'
          },
          {
            label: 'FontAwesome Icons',
            route: '/ui/icons/fa'
          }
        ]
      },
      {
        label: 'Page Layouts',
        icon: icViewCompact,
        children: [
          {
            label: 'Card',
            children: [
              {
                label: 'Default',
                route: '/ui/page-layouts/card',
                routerLinkActive: { exact: true }
              },
              {
                label: 'Tabbed',
                route: '/ui/page-layouts/card/tabbed',
              },
              {
                label: 'Large Header',
                route: '/ui/page-layouts/card/large-header',
                routerLinkActive: { exact: true }
              },
              {
                label: 'Tabbed & Large Header',
                route: '/ui/page-layouts/card/large-header/tabbed'
              }
            ]
          },
          {
            label: 'Simple',
            children: [
              {
                label: 'Default',
                route: '/ui/page-layouts/simple',
                routerLinkActive: { exact: true }
              },
              {
                label: 'Tabbed',
                route: '/ui/page-layouts/simple/tabbed',
              },
              {
                label: 'Large Header',
                route: '/ui/page-layouts/simple/large-header',
                routerLinkActive: { exact: true }
              },
              {
                label: 'Tabbed & Large Header',
                route: '/ui/page-layouts/simple/large-header/tabbed'
              }
            ]
          },
          {
            label: 'Blank',
            icon: icPictureInPicture,
            route: '/ui/page-layouts/blank'
          },
        ]
      },
      {
        label: 'Documentation'
      },
      {
        label: 'Changelog',
        route: '/documentation/changelog',
        icon: icUpdate
      },
      {
        label: 'Getting Started',
        icon: icBook,
        children: [
          {
            label: 'Introduction',
            route: '/documentation/introduction',
            fragment: 'introduction',
            routerLinkActive: { exact: true }
          },
          {
            label: 'Folder Structure',
            route: '/documentation/folder-structure',
            fragment: 'folder-structure',
            routerLinkActive: { exact: true }
          },
          {
            label: 'Installation',
            route: '/documentation/installation',
            fragment: 'installation',
            routerLinkActive: { exact: true }
          },
          {
            label: 'Development Server',
            route: '/documentation/start-development-server',
            fragment: 'start-development-server',
            routerLinkActive: { exact: true }
          },
          {
            label: 'Build for Production',
            route: '/documentation/build-for-production',
            fragment: 'build-for-production',
            routerLinkActive: { exact: true }
          }
        ]
      },
      {
        label: 'Customization',
        icon: icBook,
        children: [
          {
            label: 'Configuration',
            route: '/documentation/configuration',
            fragment: 'configuration',
            routerLinkActive: { exact: true }
          },
          {
            label: 'Changing Styling',
            route: '/documentation/changing-styling-and-css-variables',
            fragment: 'changing-styling-and-css-variables',
            routerLinkActive: { exact: true }
          },
          {
            label: 'Using Custom Colors',
            route: '/documentation/using-custom-colors-for-the-primarysecondarywarn-palettes',
            fragment: 'using-custom-colors-for-the-primarysecondarywarn-palettes',
            routerLinkActive: { exact: true }
          },
          {
            label: 'Adding Menu Items',
            route: '/documentation/adding-menu-items',
            fragment: 'adding-menu-items',
            routerLinkActive: { exact: true }
          },
        ]
      },
      {
        label: 'Further Help',
        icon: icBook,
        route: '/documentation/further-help',
        fragment: 'further-help',
        routerLinkActive: { exact: true }
      },
      {
        label: 'Customize'
      },
      {
        label: 'Open Config Panel',
        route: () => this.layoutService.openConfigpanel(),
        icon: icSettings
      }
    ];
  }
}
