import {Component, OnInit, TemplateRef, ViewChild, ViewEncapsulation} from '@angular/core';
import { Subject } from 'rxjs';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarView } from 'angular-calendar';
import { addDays, addHours, endOfDay, endOfMonth, isSameDay, isSameMonth, startOfDay, subDays } from 'date-fns';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CalendarEditComponent } from './calendar-edit/calendar-edit.component';
import icChevronLeft from '@iconify/icons-ic/twotone-chevron-left';
import icChevronRight from '@iconify/icons-ic/twotone-chevron-right';
import { CSSVariable } from '../../../../@vex/interfaces/css-variable.enum';
import {Evento} from '../../../entity/evento';
import {EventsService} from '../../../services/events.service';
import {BudgetsService} from '../../../services/budgets.service';
import {Budget} from '../../../entity/budget';
import * as moment from 'moment';

const colors: any = {
  blue: {
    primary: CSSVariable['color-primary-500'],
    secondary: CSSVariable['color-primary-contrast-500']
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};

@Component({
  selector: 'vex-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: [
    '../../../../../node_modules/angular-calendar/css/angular-calendar.css',
    './calendar.component.scss',
  ],
  encapsulation: ViewEncapsulation.None
})
export class CalendarComponent implements OnInit{

  @ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any>;

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  viewDate: Date = new Date();
  icChevronLeft = icChevronLeft;
  icChevronRight = icChevronRight;

  modalData: {
    action: string;
    event: CalendarEvent;
  };
  refresh: Subject<any> = new Subject();
  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent('Deleted', event);
      }
    }
  ];
  events: CalendarEvent[] = [
    {
      start: subDays(startOfDay(new Date()), 1),
      end: addDays(new Date(), 1),
      title: 'A 3 day event',
      color: colors.red,
      actions: this.actions,
      allDay: true,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: startOfDay(new Date()),
      title: 'An event with no end date',
      color: colors.yellow,
      actions: this.actions
    },
    {
      start: subDays(endOfMonth(new Date()), 3),
      end: addDays(endOfMonth(new Date()), 3),
      title: 'A long event that spans 2 months',
      color: colors.blue,
      allDay: true
    },
    {
      start: addHours(startOfDay(new Date()), 2),
      end: new Date(),
      title: 'A draggable and resizable event',
      color: colors.yellow,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    }
  ];
  activeDayIsOpen: boolean = true;


  // PassWheel

  budgetsLoaded = false;
  eventsLoaded = false;
  eventos: Evento[];
  budgets: Budget[];

  constructor(private dialog: MatDialog,
              private snackbar: MatSnackBar,
              private eventService: EventsService,
              private budgetsService: BudgetsService) {}

  ngOnInit(): void {
    const self = this;
    self.eventService.getSubscribeEventos().subscribe(evts => {
        self.eventos = evts ? evts : [];
        console.log(self.eventos);
        self.eventsLoaded = true;
        self.setCalendarEvents();
    });
    self.budgetsService.getAllBudgets().subscribe(bgs => {
      self.budgets = bgs ? bgs : [];
      console.log(self.budgets);
      self.budgetsLoaded = true;
      self.setCalendarEvents();
    });
    this.budgetsService.bind();
    this.eventService.bind();

  }

  setCalendarEvents() {
    if (this.budgetsLoaded && this.eventsLoaded) {
      this.events = [];
      this.eventos.forEach( evt => {
        const aux = {
          start: evt.eventInfo.dateFrom.toDate(),
          end: evt.eventInfo.dateTo.toDate(),
          title: moment(evt.eventInfo.dateFrom.toDate()).format('HH:mm') + 'hs ' + evt.eventInfo.name,
          color: colors.blue,
          actions: this.actions,
          data: evt,
          type: 'event',
        };
        this.events.push(aux);
      });
      this.budgets.forEach( bud => {
        const aux2 = {
          start: bud.dateFrom.toDate(),
          end: bud.dateTo.toDate(),
          title: moment(bud.dateFrom.toDate()).format('HH:mm') + 'hs ' + bud.name + ' ( presupuesto )',
          color: colors.yellow,
          actions: this.actions,
          data: bud,
          type: 'budget',
        };
        if (bud.state !== 'ok') { this.events.push(aux2); }
      });
    }

  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.activeDayIsOpen = !((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) || events.length === 0);
      this.viewDate = date;
    }
  }

  eventTimesChanged({ event, newStart, newEnd }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map(iEvent => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd
        };
      }
      return iEvent;
    });
    this.handleEvent('Dropped or resized', event);
  }

  handleEvent(action: string, event: CalendarEvent): void {
    console.log(event);

    const dialogRef = this.dialog.open(CalendarEditComponent, {
      data: event
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        event = result;
        this.snackbar.open('Updated Event: ' + event.title);
        this.refresh.next();
      }
    });
  }

  addEvent(): void {
    this.events = [
      ...this.events,
      {
        title: 'New event',
        start: startOfDay(new Date()),
        end: endOfDay(new Date()),
        color: colors.red,
        draggable: true,
        resizable: {
          beforeStart: true,
          afterEnd: true
        }
      }
    ];
  }

  deleteEvent(eventToDelete: CalendarEvent) {
    this.events = this.events.filter(event => event !== eventToDelete);
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }


}
