import { Component, OnInit } from '@angular/core';
import icContacts from '@iconify/icons-ic/twotone-contacts';
import icSearch from '@iconify/icons-ic/twotone-search';
import icViewHeadline from '@iconify/icons-ic/twotone-view-headline';
import icPersonAdd from '@iconify/icons-ic/twotone-person-add';
import icHistory from '@iconify/icons-ic/twotone-history';
import icLabel from '@iconify/icons-ic/twotone-label';
import icStar from '@iconify/icons-ic/twotone-star';
import { scaleIn400ms } from '../../../../../@vex/animations/scale-in.animation';
import { fadeInRight400ms } from '../../../../../@vex/animations/fade-in-right.animation';
import { TableColumn } from '../../../../../@vex/interfaces/table-column.interface';
import { contactsData } from '../../../../../static-data/contacts';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { stagger40ms } from '../../../../../@vex/animations/stagger.animation';
import { MatDialog } from '@angular/material/dialog';
import { ContactsEditComponent } from '../components/contacts-edit/contacts-edit.component';
import { Contact } from '../interfaces/contact.interface';

@Component({
  selector: 'vex-contacts-table',
  templateUrl: './contacts-table.component.html',
  styleUrls: ['./contacts-table.component.scss'],
  animations: [
    stagger40ms,
    scaleIn400ms,
    fadeInRight400ms
  ]
})
export class ContactsTableComponent implements OnInit {

  searchCtrl = new FormControl();

  searchStr$ = this.searchCtrl.valueChanges.pipe(
    debounceTime(10)
  );

  activeCategory: 'frequently' | 'starred' | 'all' | 'family' | 'friends' | 'colleagues' | 'business' = 'all';
  tableData = contactsData;
  tableColumns: TableColumn<Contact>[] = [
    {
      label: '',
      property: 'selected',
      type: 'checkbox',
      cssClasses: ['w-24']
    },
    {
      label: '',
      property: 'imageSrc',
      type: 'image',
      cssClasses: ['w-40']
    },
    {
      label: 'NAME',
      property: 'name',
      type: 'text',
      cssClasses: ['font-medium']
    },
    {
      label: 'EMAIL',
      property: 'email',
      type: 'text',
      cssClasses: ['text-secondary']
    },
    {
      label: 'PHONE',
      property: 'phone',
      type: 'text',
      cssClasses: ['text-secondary']
    },
    {
      label: '',
      property: 'starred',
      type: 'button',
      cssClasses: ['text-secondary', 'w-40']
    },
    {
      label: '',
      property: 'menu',
      type: 'button',
      cssClasses: ['text-secondary', 'w-40']
    },
  ];

  icStar = icStar;
  icPersonAdd = icPersonAdd;
  icSearch = icSearch;
  icContacts = icContacts;
  icViewHeadline = icViewHeadline;
  icHistory = icHistory;
  icLabel = icLabel;

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
  }

  setFilter(category: 'frequently' | 'starred' | 'all' | 'family' | 'friends' | 'colleagues' | 'business') {
    this.activeCategory = category;

    if (category === 'starred') {
      this.tableData = contactsData.filter(c => c.starred);
    }

    if (category === 'all') {
      this.tableData = contactsData;
    }

    if (category === 'frequently'
      || category === 'family'
      || category === 'friends'
      || category === 'colleagues'
      || category === 'business') {
      this.tableData = [];
    }
  }

  isActive(category: 'frequently' | 'starred' | 'all' | 'family' | 'friends' | 'colleagues' | 'business') {
    return this.activeCategory === category;
  }

  openContact(id?: Contact['id']) {
    this.dialog.open(ContactsEditComponent, {
      data: id || null,
      width: '600px'
    });
  }

  toggleStar(id: Contact['id']) {
    const contact = this.tableData.find(c => c.id === id);

    if (contact) {
      contact.starred = !contact.starred;
    }
  }
}
