import { CSSVariable } from '../../../../../@vex/interfaces/css-variable.enum';

export interface ScrumboardLabel {
  label: string;
  background: CSSVariable;
  color: CSSVariable;
}
