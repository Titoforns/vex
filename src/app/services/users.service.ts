import {Injectable} from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import {BehaviorSubject, Subscription} from 'rxjs';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import {Acreditator, User, UserXEvent} from '../entity/user';
import {Sessioninfo} from '../entity/sessioninfo';

@Injectable()

export class UsersService {

    sessionInfo: Sessioninfo;
    subscibedUser: BehaviorSubject<User[]> = new BehaviorSubject<User[]>(null);
    subscribedAcreditators: BehaviorSubject<Acreditator[]> = new BehaviorSubject<Acreditator[]>(null);
    subscribedUserXEvent: BehaviorSubject<UserXEvent[]> = new BehaviorSubject<UserXEvent[]>(null);

    private tableUsers = 'users';
    private tableAcreditators = 'acreditators';
    private tableUserXEvent = 'usersxevents';

    private doc: AngularFirestoreDocument<Event>;

    subsUser?: Subscription = null;
    subsAcreditators?: Subscription = null;
    subsUserXEvent?: Subscription = null;

    public listUsers?: User[] = [];
    public listAcreditators?: Acreditator[] = [];
    public listUserXEvent?: UserXEvent[] = [];

    constructor(private af: AngularFirestore, private aFDB: AngularFireDatabase) {
        this.sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
    }

    getAcreditators() {
        return this.subscribedAcreditators.asObservable();
    }

    getUserXEvent(idEvent: string, idAcreditator: string) {
        // return this.subscribedUserXEvent.asObservable();
        const self = this;
        if (self.subscribedUserXEvent != null) {
            this.unbindUsersXEvent();
        }
        let userXEvent: UserXEvent = null;
        return new Promise( (resolve, reject) => {
            self.subsUserXEvent = self.af.collection<UserXEvent>(self.tableUserXEvent, p =>
                p.where('idEvent', '==', idEvent)
                    .where('idAcreditator', '==', idAcreditator)
                    .limit(100)).snapshotChanges().subscribe(res => {
                userXEvent = null;
                res.map(p => {
                    const d = <UserXEvent>p.payload.doc.data();
                    userXEvent = d;
                });
                resolve(userXEvent);
            }, error => reject(null));
        }).catch( err => console.error(err));
    }

    getUsers() {
        return this.subscibedUser.asObservable();
    }

    savePassword(user: User, pass: string) {
        user.password = pass;
        const b = JSON.parse(JSON.stringify(user));
        this.doc = this.af.doc(`${this.tableUsers}/${user.id}`);
        this.doc.update(b).then( () => {});
    }

    getUser(uid: string) {
        return this.af
            .doc(`${this.tableUsers}/${uid}`)
            .get()
            .toPromise()
            .then(snap => {
                const data = snap.data() as User;
                return data;
            });
    }

    createUser(uid: string, email: string) {
        const user = new User();
        user.type = 'admin';
        user.id = uid;
        user.userName = email;
        user.state = true;
        const b = JSON.parse(JSON.stringify(user));
        this.doc = this.af.doc(`${this.tableUsers}/${user.id}`);
        return this.doc.set(b);
    }

    addAcreditator(a: Acreditator) {
        const self = this;
        this.sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
        const b = JSON.parse(JSON.stringify(a));
        b.id = self.af.createId();
        b.idUser = this.sessionInfo.userCode;
        this.doc = this.af.doc(`${this.tableAcreditators}/${b.id}`);
        return this.doc.set(b);
    }

    addUserXEvent(a: UserXEvent) {
        const self = this;
        const b = JSON.parse(JSON.stringify(a));
        b.id = self.af.createId();
        this.doc = this.af.doc(`${this.tableUserXEvent}/${b.id}`);
        this.doc.set(b).then( () => {});
    }

    getAcreditatorById(id: string): Promise<Acreditator | null> {
        const self = this;
        if (self.subscribedUserXEvent != null) {
            this.unbindAcreditators();
        }
        return new Promise( (resolve, reject) => {
            self.subsAcreditators = self.af.collection<Acreditator>(self.tableAcreditators, a =>
                a.where('idUser', '==', id)
                    .limit(100)).snapshotChanges().subscribe(acred => {
                acred.map(a => {
                    const k = <Acreditator>a.payload.doc.data();
                    resolve(k);
                });
                reject(null);
            });
        });
       }

    bindUserXEvent(idEvent: string) {
        const self = this;
        this.sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
        if (self.subscribedUserXEvent != null) {
            this.unbindUsersXEvent();
        }
        self.listUserXEvent = [];
        self.subsUserXEvent = self.af.collection<UserXEvent>(self.tableUserXEvent, p =>
            p.where('idEvent', '==', idEvent)
                .limit(100)).snapshotChanges().subscribe(res => {
            self.listUserXEvent = [];
            res.map(p => {
                const d = <UserXEvent>p.payload.doc.data();
                self.listUserXEvent.push(d);
            });
            this.subscribedUserXEvent.next(self.listUserXEvent);
        });
    }


    bindAcreditatorByUser() {
        const self = this;
        this.sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
        if (self.subsAcreditators != null) {
            this.unbindAcreditators();
        }
        self.listAcreditators = [];
        self.subsAcreditators = self.af.collection<Acreditator>(self.tableAcreditators, p =>
            p.where('idUser', '==', self.sessionInfo.userCode)
                .limit(100)).snapshotChanges().subscribe(res => {
            self.listAcreditators = [];
            res.map(p => {
                const d = <Acreditator>p.payload.doc.data();
                self.listAcreditators.push(d);
            });
            this.subscribedAcreditators.next(self.listAcreditators);
        });
    }

    unbindUsersXEvent() {
        const self = this;
        if (self.subsUserXEvent == null) {
            return;
        }
        self.subsUserXEvent.unsubscribe();
        self.subsUserXEvent = null;
        self.listUserXEvent = [];

    }

    unbindAcreditators() {
        const self = this;
        if (self.subsAcreditators == null) {
            return;
        }
        self.subsAcreditators.unsubscribe();
        self.subsAcreditators = null;
        self.subscribedAcreditators.next(null);
        self.listAcreditators = [];

    }

    upLoadPhoto(file: File, user: User) {
        // Create a root reference
        const storageRef = firebase.storage().ref();

        // Create a reference to 'mountains.jpg'
        const mountainsRef = storageRef.child('mountains.jpg');

        // Create a reference to 'images/mountains.jpg'
        const mountainImagesRef = storageRef.child('images/mountains.jpg');

        // While the file names are the same, the references point to different files
        // mountainsRef.name === mountainImagesRef.name            // true
        // mountainsRef.fullPath === mountainImagesRef.fullPath    // false

        // File or Blob named mountains.jpg

        // Create the file metadata
        const metadata = {
            contentType: 'image/jpeg'
        };

        // Upload file and metadata to the object 'images/mountains.jpg'
        const uploadTask = storageRef.child('images/' + file.name).put(file, metadata);

        /*
        // Listen for state changes, errors, and completion of the upload.
        uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
            function(snapshot) {
                // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                console.log('Upload is ' + progress + '% done');
                switch (snapshot.state) {
                    case firebase.storage.TaskState.PAUSED: // or 'paused'
                        console.log('Upload is paused');
                        break;
                    case firebase.storage.TaskState.RUNNING: // or 'running'
                        console.log('Upload is running');
                        break;
                }
            }, function(error) {

                // A full list of error codes is available at
                // https://firebase.google.com/docs/storage/web/handle-errors
                switch (error.code) {
                    case 'storage/unauthorized':
                        // User doesn't have permission to access the object
                        break;

                    case 'storage/canceled':
                        // User canceled the upload
                        break;

                    ...

                    case 'storage/unknown':
                        // Unknown error occurred, inspect error.serverResponse
                        break;
                }
            }, function() {
                // Upload completed successfully, now we can get the download URL
                uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
                    console.log('File available at', downloadURL);
                });
            });
            */
    }
}
