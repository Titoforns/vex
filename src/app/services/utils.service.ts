import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import { AngularFireDatabase } from 'angularfire2/database';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {MatSnackBar} from '@angular/material';
import * as moment from 'moment';
import {Sessioninfo} from '../entity/sessioninfo';
import {Evento} from '../entity/evento';

@Injectable()

export class UtilsService {

    public drawerOpen: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    public currentPage: BehaviorSubject<string> = new BehaviorSubject<string>('splash');
    public pageLoading: BehaviorSubject<string> = new BehaviorSubject<string>(null);
    public barLoading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    sessionInfo: Sessioninfo;

    constructor(
                private _snackBar: MatSnackBar,
                private aFDB: AngularFireDatabase,
                private http: HttpClient) {

    }

    getDrawerOpen(): Observable<boolean> {
        return this.drawerOpen.asObservable();
    }

    setDrawerOpen(bool: boolean): void {
        this.drawerOpen.next(bool);
    }

    getCurrentPage(): Observable<string> {
        return this.currentPage.asObservable();
    }

    setCurrentPage(page: string) {
        this.currentPage.next(page);
    }

    loading(mssg?: string) {
        this.pageLoading.next(mssg ? mssg : 'Cargando...');
        setTimeout( () => {
            this.pageLoading.next(null);
        }, 10000);
    }

    unLoading() {
        this.pageLoading.next(null);
    }

    getLoading() {
        return this.pageLoading.asObservable();
    }

    getBarLoading() {
        return this.barLoading.asObservable();
    }

    setBarLoading(bool: boolean) {
        this.barLoading.next(bool);
    }

    sendMail(eventName, mail, mssg) {
        const mailObject = {
            eventName: eventName,
            mssg: mssg
        };
        return this.http.post('https://us-central1-contacts21.cloudfunctions.net/sendPasswheelMail?dest=' + mail + '&data=' + JSON.stringify(mailObject), {});
    }

    sendMailToAcreditator(mail, e: Evento) {
        this.sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
        moment.locale('es');
        const day  = moment(e.eventInfo.dateFrom.toDate()).locale('es').format('dddd D [de] MMMM [a las] HH:00 [hs]');
        const mailObject = {
            link: 'http://localhost:4200/login', //  + this.sessionInfo.userCode + '&idEvent=' + e.id,
            eventName: e.eventInfo.name,
            dateFrom: day,
            place: e.eventInfo.location,
            eventCode: e.code,
            eventPassword: e.password
        };
        return this.http.post('https://us-central1-contacts21.cloudfunctions.net/sendToAcreditator?dest=' + mail + '&data=' + JSON.stringify(mailObject), {});
    }

    convertObjectArrayToArray(obj: any ) {
        return Object.keys(obj).map(key => {
            const ar = obj[key]
            return ar;
        });
    }



}
