import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NewTrelloCard, TrelloCard} from '../entity/trello-card';

@Injectable()

export class TrelloService {

    // Api key de passwheel
    // apiKey = '066550adf304d190ca15d8196ae63efa';

    // Api key de trazzo
    apiKey = '5f9bc36f32879ddfadbe4bbf734d9691';

    // Token de passwheel
    // token = 'f8e76ed7899a7ca46bcf01c8ddf1043251ae874a3e74249e857af4283b340966';

    // Token de trazzo
    token = '0a8b79b90f8b580f23332749e77d99170210f763d0b508bc5a4b82c1e2755d82';

    // Board Passwheel
    // boardKey = 'QyjHbsZ7';

    // Board eventtos (trazzo)
    boardKey = 'QyjHbsZ7';

    basicUrl = 'https://api.trello.com/1/';

    constructor(private http: HttpClient) {
    }

    getTrelloCards() {
        return this.http.get(this.basicUrl + 'boards/' + this.boardKey + '/cards?key=' + this.apiKey + '&token=' + this.token);
    }

    getInfoCard(listKey: string) {
        return this.http.get(this.basicUrl + 'lists/' + listKey + '?key=' + this.apiKey + '&token=' + this.token);
    }

    updateCard(card: TrelloCard) {
        return this.http.put(this.basicUrl + 'cards/' + card.id + '?key=' + this.apiKey + '&token=' + this.token, card);
    }

    addCard(card: NewTrelloCard) {
        return this.http.post(this.basicUrl + 'cards?idList=' + card.idList + '&keepFromSource=all&key=' + this.apiKey + '&token=' + this.token, card);
    }

}
