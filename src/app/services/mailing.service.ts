import {Injectable} from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import {BehaviorSubject, Subscription} from 'rxjs';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import {AuthenticationService} from './authentication.service';
import {HttpClient} from '@angular/common/http';
import {Evento} from '../entity/evento';

@Injectable()

export class MailingService {

    private nameTable = 'mailing';
    private doc: AngularFirestoreDocument<Event>;
    subscribedMails: BehaviorSubject<any[]> = new BehaviorSubject<any[]>(null);
    addMails: BehaviorSubject<any[]> = new BehaviorSubject<any[]>(null);

    subscriptionList?: Subscription = null;
    public list?: Evento[] = [];

    constructor(private http: HttpClient, private auth: AuthenticationService, private af: AngularFirestore, private aFDB: AngularFireDatabase) {
    }

    getCurrentMails() {
        return this.subscribedMails.asObservable();
    }

    setAddMails(list: any[]) {
        this.addMails.next(list);
    }

    getAddMails() {
        return this.addMails.asObservable();
    }

    getMails(code) {
        const self = this;
        self.subscriptionList = self.af.collection<Evento>(self.nameTable, p =>
            p.where('idEvent', '==', code)
                .limit(3000)).snapshotChanges().subscribe(res => {
            let d = null;
            const list = [];
            res.map(p => {
                d = <Evento>p.payload.doc.data();
                list.push(d);
            });
            self.subscribedMails.next(list);
        });
    }

    updateMail(m) {
        const b = JSON.parse(JSON.stringify(m));
        this.af
            .doc(`${this.nameTable}/${m.id}`)
            .update(b).then( r => console.log('Updated: ' + r));
    }

    addMail(m: any) {
        const self = this;
        m.id = self.af.createId();
        const b = JSON.parse(JSON.stringify(m));

        return this.af
            .doc(`${this.nameTable}/${m.id}`)
            .set(b)
            .then(snap => {
                return m.id;
            });
    }

    addMailAsync(m) {
        return new Promise( (resolve, reject) => {
            const self = this;
            m.id = self.af.createId();
            const b = JSON.parse(JSON.stringify(m));

            return this.af
                .doc(`${this.nameTable}/${m.id}`)
                .set(b)
                .then( () => {
                    resolve(true);
                }).catch( () => reject(false));
        });
    }

    add(o: any) {
        const self = this;
        o.id = self.af.createId();
        const b = JSON.parse(JSON.stringify(o));

        return this.af
            .doc(`${this.nameTable}/${o.id}`)
            .set(b)
            .then(snap => {
                return o.id;
            });
    }

    unbind() {
        const self = this;
        if (self.subscriptionList) { self.subscriptionList.unsubscribe(); }
        self.subscribedMails.next(null);
        self.addMails.next(null);
    }

    sendAccountRequestMail(email: string, mssg: string, account: string, ev: Evento) {
        return new Promise( (resolve, reject) => {
            const obj = {
                email: email,
                mssg: mssg,
                account: account,
                eventName: ev.eventInfo.name,
                eventId: ev.id
            };
            this.http.post('https://us-central1-contacts21.cloudfunctions.net/sendMailAccount', JSON.stringify(obj)).subscribe( r => {
                resolve(r);
            }, e => {
                if (e.status === 200) {
                    resolve('Enviado');
                } else {
                    reject(e);
                }
            });
        });
    }

    sendAsyncInvitation(obj, dest, body) {
        return new Promise( (resolve, reject) => {
            this.http.post('https://us-central1-contacts21.cloudfunctions.net/sendMailInvitation?dest=' + dest + '&data=' + JSON.stringify(obj), JSON.stringify(body)).subscribe( r => {
                resolve(r);
            }, e => {
                if (e.status === 200) {
                    resolve('Enviado');
                } else {
                    reject(e);
                }
            });
        });
    }

    sendInvitation(obj, dest, body) {
        return new Promise( (resolve, reject) => {
            this.http.post('https://us-central1-contacts21.cloudfunctions.net/sendMailInvitation?dest=' + dest + '&data=' + JSON.stringify(obj), JSON.stringify(body)).subscribe( data => {
                resolve('Enviadooo');
            }, e => {
                if (e.status === 200) {
                    resolve('Enviado');
                } else {
                    reject(e);
                }
            });
        });
    }
}
