import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {
    AngularFirestore,
    AngularFirestoreDocument
} from '@angular/fire/firestore';
import { AuthProvider } from '@firebase/auth-types';
import * as firebase from 'firebase/app';
import { AngularFireStorage } from '@angular/fire/storage';
import {BehaviorSubject, Observable} from 'rxjs';
import {Router} from '@angular/router';
import {Sessioninfo} from '../entity/sessioninfo';
import {User} from '../entity/user';


@Injectable()
export class AuthenticationService {

    public sessionInfo: BehaviorSubject<Sessioninfo> = new BehaviorSubject<Sessioninfo>(null);
    public logged = false;

    constructor(
        private oauth: AngularFireAuth,
        private af: AngularFirestore,
        private router: Router,
        private storage: AngularFireStorage
    ) {
        const self = this;

        self.oauth.auth.onAuthStateChanged(user => {
            if (user == null) {
                self.logout();
                return;
            }
            this.af
                .doc(`${this.nameTable}/${user.uid}`)
                .get().toPromise()
                .then(us => {
                    this.logged = true;
                }).catch(reason => {
                    self.logout();
                });
        });
    }
    private _auth = false;
    private nameTable = 'users';
    private _user: User = null;
    private doc: AngularFirestoreDocument<User>;

    logout() {
        const self = this;
        self.oauth.auth
            .signOut()
            .then(() => {
                self._user = null;
                self._auth = false;
                self.logged = false;
                this.clearLocalStorage();
                this.router.navigate(['admin']);
                // document.location.reload(false);
            })
            .catch((reason) => {
                console.error(reason);
            });
    }

    getSessionInfo(): Observable<Sessioninfo> {
        return this.sessionInfo.asObservable();
    }

    setSessionInfo(session: Sessioninfo) {
        localStorage.setItem('sessionInfo', JSON.stringify(session));
        this.sessionInfo.next(session);
    }

    getUser() {
        return this._user;
    }

    setLogged(bool) {
        this.logged = bool;
    }

    getLogged(): boolean {
        return this.logged;
    }

    getIsUserLogged() {
        return this._auth;
    }

    signUp(email, password) {
        const self = this;
        return this.oauth.auth.createUserWithEmailAndPassword(email, password)
            .then((result) => {
                console.log(result);
            }).catch((error) => {
                console.error(error);
            });
    }

    loginEmailAndPassword(email: string, pass: string) {
        return this.oauth.auth.signInWithEmailAndPassword(email, pass);
    }

    createUserWithEmailAndPassword(email: string, pass: string) {
        return this.oauth.auth.createUserWithEmailAndPassword(email, pass);
    }

    addUser(user: User) {
        const self = this;
        return new Promise( (resolve, reject) => {
            self.oauth.auth.createUserWithEmailAndPassword(user.userName, user.password).then( (data: any) => {
                console.log(data);
                user.id = data.user.uid;
                self.add(user).then( dat => {
                    self._user = user;
                    console.log(dat);
                    resolve(true);
                }).catch( error => reject(error));
            }).catch( err => reject(err));
        }).catch( e =>  console.log(e));
    }

    loginGoogle() {
        return this.oauthSignIn(new firebase.auth.GoogleAuthProvider());
    }



    sendRecoverPassword(email) {
        return this.oauth.auth.sendPasswordResetEmail(email);
    }

    clearLocalStorage() {
        localStorage.setItem('sessionInfo', null);
        localStorage.setItem('eventInfo', null);
        localStorage.setItem('user', null);
        this.sessionInfo.next(null);
    }

    private oauthSignIn(provider: AuthProvider) {
            return new Promise((resolve, reject) => {
                this.oauth.auth
                    .signInWithRedirect(provider)
                    .then(() => {
                        return this.oauth.auth
                            .getRedirectResult()
                            .then(result => {
                                // This gives you a Google Access Token.
                                // You can use it to access the Google API.
                                // The signed-in user info.
                                const user = result.user;

                                // console.log(token, user);
                                resolve();
                            })
                            .catch(function (error) {
                                // Handle Errors here.
                                reject(error);
                            });
                    })
                    .catch(function (error) {
                        // Handle Errors here.
                        reject(error);
                    });
            });
    }

    private createOrUpdateUser(user: firebase.User, snapUser: any): Promise<User> {
        console.log(user, snapUser);
        const self = this;
        return new Promise((resolve, reject) => {
            if (snapUser == null || snapUser.data() == null) {
                let name = '';
                let surname = '';
                const parts = user.displayName == null ? [] : user.displayName.split(' ');
                if (parts.length > 1) {
                    name = parts[0];
                    surname = user.displayName.replace(`${name} `, '');
                } else {
                    name = user.displayName == null ? 'noname' : user.displayName;
                    surname = '';
                }

                const dbuser: User = {
                    id: user.uid,
                    name: name,
                    lastName: surname != null ? surname : null,
                    userName: user.email != null ? user.email : null,
                    userPhoto: user.photoURL !== undefined ? user.photoURL : null,
                    type: 'acreditator',
                    state: false,
                    password: '',
                    account: 'basic'
                };
                self.af.doc(`${self.nameTable}/${user.uid}`).set(dbuser).then(() => {
                    self._user = dbuser;
                    resolve();
                }).catch(reject);
                return;
            }
            self._user = <User>snapUser.data();
            self._user.id = snapUser.id;
            resolve();
        });
    }

    get(id: string) {
        return this.af
            .doc(`${this.nameTable}/${id}`)
            .get()
            .toPromise()
            .then(snap => {
                const data = snap.data() as User;
                data.id = snap.id;
                return data;
            });
    }

    add(o: User) {
        o.id = o.id === '' ? this.af.createId() : o.id;
        const b = JSON.parse(JSON.stringify(o));
        this.doc = this.af.doc(`${this.nameTable}/${o.id}`);
        return this.doc.set(b);
    }

    update(o: User) {
        const b = JSON.parse(JSON.stringify(o));
        this.doc = this.af.doc(`${this.nameTable}/${o.id}`);
        return this.doc.update(b);
    }

    addOrUpdate(o: User) {
        if (o.id == undefined || o.id == '') {
            return this.add(o);
        }
        return this.update(o);
    }

    getUsersTechnicals() {

        const self = this;
        const col = self.af.collection(self.nameTable, p => p.where('active', '==', true).orderBy('surname', 'asc'));

        return col.get().toPromise().then(p => {
            const ar = [];
            p.docs.forEach(doc => {
                const data = doc.data() as User;
                data.id = doc.id;

                ar.push(data);
            });
            console.log(ar);
            return ar;
        });
    }

}
