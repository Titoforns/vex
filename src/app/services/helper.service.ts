import {Injectable} from '@angular/core';
import * as $ from 'jquery';

@Injectable()

export class HelperService {

    constructor() {}

    initTour() {
        const self = this;
        console.log('iniciadooo');
        const tourWrapper = $('.cd-tour-wrapper'),
            tourSteps = tourWrapper.children('li'),
            stepsNumber = tourSteps.length,
            coverLayer = $('.cd-cover-layer'),
            tourStepInfo = $('.cd-more-info'),
            tourTrigger = $('#cd-tour-trigger');

        // create the navigation for each step of the tour
        self.createNavigation(tourSteps, stepsNumber);

        tourTrigger.on('click', function() {
            // start tour
            if (!tourWrapper.hasClass('active')) {
                // in that case, the tour has not been started yet
                tourWrapper.addClass('active');
                self.showStep(tourSteps.eq(0), coverLayer);
            }
        });

        // change visible step
        tourStepInfo.on('click', '.cd-prev', function(event) {
            // go to prev step - if available
            ( !$(event.target).hasClass('inactive') ) && self.changeStep(tourSteps, coverLayer, 'prev');
        });
        tourStepInfo.on('click', '.cd-next', function(event) {
            // go to next step - if available
            ( !$(event.target).hasClass('inactive') ) && self.changeStep(tourSteps, coverLayer, 'next');
        });

        // close tour
        tourStepInfo.on('click', '.cd-close', function(event) {
            self.closeTour(tourSteps, tourWrapper, coverLayer);
        });

        // detect swipe event on mobile - change visible step
        tourStepInfo.on('swiperight', function(event) {
            // go to prev step - if available
            if ( !$(this).find('.cd-prev').hasClass('inactive') && self.viewportSize() === 'mobile' ) self.changeStep(tourSteps, coverLayer, 'prev');
        });
        tourStepInfo.on('swipeleft', function(event) {
            // go to next step - if available
            if ( !$(this).find('.cd-next').hasClass('inactive') && self.viewportSize() === 'mobile' ) self.changeStep(tourSteps, coverLayer, 'next');
        });

    }

    createNavigation(steps, n) {
        const tourNavigationHtml = '<div class="cd-nav" style="display: flex; justify-content: space-between;"><span><b class="cd-actual-step">1</b> de ' + n + '</span><ul class="cd-tour-nav" style="width: 60%; list-style: none; display: flex;justify-content: space-between; "><li><a style="text-decoration: none" href="javascript: void()" class="cd-prev">&#171; Previous</a></li><li><a style=" text-decoration: none" href="javascript: void()" class="cd-next">Next &#187;</a></li></ul></div><a style="opacity: .5; position: absolute; top: 15px; right: 15px; text-decoration: none" href="javascript: void()" class="cd-close">Cerrar</a>';

        steps.each(function(index) {
            const step = $(this),
                stepNumber = index + 1,
                nextClass = ( stepNumber < n ) ? '' : 'inactive',
                prevClass = ( stepNumber === 1 ) ? 'inactive' : '';
            const nav = $(tourNavigationHtml).find('.cd-next').addClass(nextClass).end().find('.cd-prev').addClass(prevClass).end().find('.cd-actual-step').html(stepNumber).end().appendTo(step.children('.cd-more-info'));
        });
    }

    showStep(step, layer) {
        step.addClass('is-selected').removeClass('move-left');
        this.smoothScroll(step.children('.cd-more-info'));
        this.showLayer(layer);
    }

    smoothScroll(element) {
        (element.offset().top < $(window).scrollTop()) && $('body,html').animate({'scrollTop': element.offset().top}, 100);
        (element.offset().top + element.height() > $(window).scrollTop() + $(window).height() ) && $('body,html').animate({'scrollTop': element.offset().top + element.height() - $(window).height()}, 100);
    }

    showLayer(layer) {
        layer.addClass('is-visible').on('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(){
            layer.removeClass('is-visible');
        });
    }

    changeStep(steps, layer, bool) {
        const self = this;
        const visibleStep = steps.filter('.is-selected'),
            delay = (self.viewportSize() === 'desktop') ? 300: 0;
        visibleStep.removeClass('is-selected');

        (bool === 'next') && visibleStep.addClass('move-left');

        setTimeout(function() {
            ( bool === 'next' )
                ? self.showStep(visibleStep.next(), layer)
                : self.showStep(visibleStep.prev(), layer);
        }, delay);
    }

    closeTour(steps, wrapper, layer) {
        steps.removeClass('is-selected move-left');
        wrapper.removeClass('active');
        layer.removeClass('is-visible');
    }

    viewportSize() {
        /* retrieve the content value of .cd-main::before to check the actua mq */
        return window.getComputedStyle(document.querySelector('.cd-tour-wrapper'), '::before').getPropertyValue('content').replace(/"/g, '').replace(/'/g, '');
    }
}