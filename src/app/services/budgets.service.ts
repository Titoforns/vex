import {Injectable} from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import {BehaviorSubject, Subscription} from 'rxjs';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import {Budget} from '../entity/budget';
import {StockService} from '../entity/stockService';
import {Sessioninfo} from '../entity/sessioninfo';


@Injectable()

export class BudgetsService {

    sessionInfo: Sessioninfo;
    subscribedBudgets: BehaviorSubject<Budget[]> = new BehaviorSubject<Budget[]>(null);
    subscribedPassBudgets: BehaviorSubject<Budget[]> = new BehaviorSubject<Budget[]>(null);
    subscribedAllBudgets: BehaviorSubject<Budget[]> = new BehaviorSubject<Budget[]>(null);
    subscribedServices: BehaviorSubject<StockService[]> = new BehaviorSubject<StockService[]>(null);

    private nameTable = 'budgets';
    private doc: AngularFirestoreDocument<Event>;

    subscriptionList?: Subscription = null;
    subscriptionListServices?: Subscription = null;
    public list?: Budget[] = [];
    public listServices?: StockService[] = [];
    public passBudgets?: Budget[] = [];
    public allBudgets?: Budget[] = [];

    constructor(private af: AngularFirestore, private aFDB: AngularFireDatabase) {
        this.sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
        this.bind();
        this.bindServices();
    }

    getBudgets() {
        return this.subscribedBudgets.asObservable();
    }

    getPassBudgets() {
        return this.subscribedPassBudgets.asObservable();
    }

    getAllBudgets() {
        return this.subscribedAllBudgets.asObservable();
    }

    getServices() {
        return this.subscribedServices.asObservable();
    }

    bind() {
        const self = this;
        this.sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
        if (self.subscriptionList != null) {
            this.unbind();
        }
        const today = new Date().getTime();
        self.list = [];
        self.passBudgets = [];
        self.allBudgets = [];
            self.subscriptionList = self.af.collection<Budget>(self.nameTable, p =>
                p.where('idUser', '==', self.sessionInfo.userCode)
                    .limit(100)).snapshotChanges().subscribe(res => {
                self.list = [];
                self.passBudgets = [];
                self.allBudgets = [];
                res.map(p => {
                    const d = <Budget>p.payload.doc.data();
                    if (d.dateFrom.toDate().getTime() >= today ) {
                        self.list.push(d);
                    } else {
                        self.passBudgets.push(d);
                    }
                    self.allBudgets.push(d);
                });
                this.subscribedBudgets.next(self.list);
                this.subscribedPassBudgets.next(self.passBudgets);
                this.subscribedAllBudgets.next(self.allBudgets);
        });
    }


    unbind() {
        const self = this;
        if (self.subscriptionList == null) {
            return;
        }
        self.subscriptionList.unsubscribe();
        self.subscriptionList = null;
        self.subscribedAllBudgets.next(null);
        self.subscribedBudgets.next(null);
        self.subscribedPassBudgets.next(null);
        self.list = [];
        self.passBudgets = [];
        self.allBudgets = [];
    }

    bindServices() {
        const self = this;
        if (self.subscriptionListServices != null) {
            this.unbindServices();
        }
        self.listServices = [];
        self.subscriptionListServices = self.af.collection<StockService>('services', p =>
            p.limit(100)).snapshotChanges().subscribe(res => {
            self.listServices = [];
            res.map(p => {
                const d = <StockService>p.payload.doc.data();
                self.listServices.push(d);
            });
            this.subscribedServices.next(self.listServices);
        });
    }


    unbindServices() {
        const self = this;
        if (self.subscribedServices == null) {
            return;
        }
        self.subscribedServices.unsubscribe();
        self.subscribedServices = null;
        self.subscribedServices.next(null);
        self.listServices = [];
    }

    add(o: Budget) {
        const self = this;
        o.id = self.af.createId();
        const b = JSON.parse(JSON.stringify(o));
        self.doc = this.af.doc(`${self.nameTable}/${o.id}`);
        b.dateFrom = o.dateFrom;
        b.dateTo = o.dateTo;
        return self.doc.set(b);
    }

    saveBudget(o: Budget) {
        const self = this;
        const b = JSON.parse(JSON.stringify(o));
        self.doc = this.af.doc(`${self.nameTable}/${o.id}`);
        b.dateFrom = o.dateFrom;
        b.dateTo = o.dateTo;
        return self.doc.update(b);
    }
}
