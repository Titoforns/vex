import {Injectable} from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import {BehaviorSubject, Subscription} from 'rxjs';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import {AuthenticationService} from './authentication.service';
import {Sessioninfo} from '../entity/sessioninfo';
import {UserXEvent} from '../entity/user';
import {Evento} from '../entity/evento';


@Injectable()

export class EventsService {

    subscribedEvents: BehaviorSubject<Evento[]> = new BehaviorSubject<Evento[]>(null);
    currentEvent: BehaviorSubject<Evento> = new BehaviorSubject<Evento>(null);

    private nameTable = 'eventos';
    private userXEventTable = 'usersxevents';
    private doc: AngularFirestoreDocument<Event>;

    subscriptionList?: Subscription = null;
    public list?: Evento[] = [];
    public listUserXEvnt?: UserXEvent[] = [];
    sessionInfo: Sessioninfo;

    subsEvent: Subscription;
    subsUserXEvent: Subscription;

    constructor(private auth: AuthenticationService, private af: AngularFirestore, private aFDB: AngularFireDatabase) {
        this.sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
    }

    getEvents()  {
        return this.aFDB.list<Evento>('events/' + this.sessionInfo.userCode + '/events/');
    }

    public getAllEvents() {
        return this.list;
    }

    getSubscribeEventos() {
        return this.subscribedEvents.asObservable();
    }

    getCurrentEvent() {
        return this.currentEvent.asObservable();
    }

    getEvent(code) {
        const self = this;
        self.subscriptionList = self.af.collection<Evento>(self.nameTable, p =>
            p.where('id', '==', code)
                .limit(100)).snapshotChanges().subscribe(res => {
            let d: Evento = null;
            res.map(p => {
                d = p.payload.doc.data();
            });
            self.currentEvent.next(d);
        });
    }

    getEventsXUser(idUser: string) {
        const self = this;
        this.sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
        if (self.subsUserXEvent != null) {
            this.unbind();
        }
        self.listUserXEvnt = [];
        return new Promise( (resolve, reject) => {
            self.subsUserXEvent = self.af.collection<UserXEvent>(self.userXEventTable, p =>
                p.where('idAcreditator', '==', idUser)
                    .limit(100)).snapshotChanges().subscribe(res => {
                self.listUserXEvnt = [];
                res.map(p => {
                    self.listUserXEvnt.push(p.payload.doc.data());
                });
                resolve(self.listUserXEvnt);
            }, error => reject(error));
        }).catch( e => console.error(e));
    }

    getEventById(idEvent: string) {
        // return this.subscribedUserXEvent.asObservable();
        const self = this;
        return new Promise( (resolve, reject) => {
            self.af.collection<Evento>(self.nameTable, p =>
                p.where('id', '==', idEvent)
                    .limit(100)).snapshotChanges().subscribe(res => {
                let eve: Evento = null;
                res.map(p => {
                    eve = p.payload.doc.data();;
                });
                resolve(eve);
            }, error => reject(null));
        }).catch( err => console.error(err));
    }

    getEventByIdCodeAndPassword(idEvent: string, eventCode: string, eventPassword: string) {
        // return this.subscribedUserXEvent.asObservable();
        const self = this;
        return new Promise( (resolve, reject) => {
            self.af.collection<Evento>(self.nameTable, p =>
                p.where('id', '==', idEvent)
                    .where('code', '==', eventCode)
                    .where('password', '==', eventPassword)
                    .limit(100)).snapshotChanges().subscribe(res => {
                let eve: Evento = null;
                res.map(p => {
                    eve = p.payload.doc.data();
                });
                resolve(eve);
            }, error => reject(null));
        }).catch( err => console.error(err));
    }

    saveEventInfo(o: Evento) {
        const b = JSON.parse(JSON.stringify(o));
        this.doc = this.af.doc(`${this.nameTable}/${o.id}`);
        b.eventInfo.dateFrom = o.eventInfo.dateFrom;
        b.eventInfo.dateTo = o.eventInfo.dateTo;
        return this.doc.update(b);
    }

    add(o: Evento) {
        const self = this;
        this.sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
        o.id = self.af.createId();
        const b = JSON.parse(JSON.stringify(o));
        // self.doc = this.af.doc(`${self.nameTable}/${o.id}`);
        b.eventInfo.dateFrom = o.eventInfo.dateFrom;
        b.eventInfo.dateTo = o.eventInfo.dateTo;
        b.idUser = self.sessionInfo.userCode;
        // return self.doc.set(b);

        return this.af
            .doc(`${this.nameTable}/${o.id}`)
            .set(b)
            .then(snap => {
                return o.id;
            });
    }

    bind() {
        const self = this;
        this.sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
        if (self.subscriptionList != null) {
            this.unbind();
        }
        self.list = [];
        self.subscriptionList = self.af.collection<Evento>(self.nameTable, p =>
            p.where('idUser', '==', self.sessionInfo.userCode)
                .limit(100)).snapshotChanges().subscribe(res => {
            self.list = [];
            res.map(p => {
                self.list.push(p.payload.doc.data());
            });
            this.subscribedEvents.next(self.list);
        });
    }

    getEventByCodeAndPassword(code: string, password: string) {
        const self = this;
        return new Promise( (resolve, reject) => {
            self.af.collection<Evento>(self.nameTable, p =>
                p.where('code', '==', code)
                    .where('password', '==', password)
                    .limit(100)).snapshotChanges().subscribe(res => {
                let eve: Evento = null;
                res.map(p => {
                    eve = p.payload.doc.data();
                });
                resolve(eve);
            }, error => reject(null));
        }).catch( err => console.error(err));
    }

    unbind() {
        const self = this;
        if (self.subscriptionList == null) {
            return;
        }
        self.subscriptionList.unsubscribe();
        self.subscriptionList = null;
        self.list = [];
    }

    unbindUserXEvent() {
        const self = this;
        if (self.subsUserXEvent == null) {
            return;
        }
        self.subsUserXEvent.unsubscribe();
        self.subsUserXEvent = null;
        self.listUserXEvnt = [];

    }

    unbindEvent() {
        const self = this;
        if (self.subsEvent == null) {
            return;
        }
        self.subsEvent.unsubscribe();
    }

    saveOrderInputs(o: Evento) {
        const b = JSON.parse(JSON.stringify(o));
        this.doc = this.af.doc(`${this.nameTable}/${o.id}`);
        b.eventInfo.dateFrom = o.eventInfo.dateFrom;
        b.eventInfo.dateTo = o.eventInfo.dateTo;
        return this.doc.update(b);
    }

    checkCode(code: string, idEvent: string) {
        const self = this;
        return new Promise( (resolve, reject) => {
            self.af.collection<Evento>(self.nameTable, p =>
                p.where('code', '==', code)
                    .limit(100)).snapshotChanges().subscribe(res => {
                let ret = true;
                res.map(p => {
                    const d: Evento = p.payload.doc.data();
                    d.id === idEvent ? ret = true : ret = false;
                });
                resolve(ret);
            }, error => reject(null));
        }).catch( err => console.error(err));
    }
}
