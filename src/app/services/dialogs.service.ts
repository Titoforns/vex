import {Component, Inject, Injectable} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar, MAT_SNACK_BAR_DATA} from '@angular/material';
import {Evento} from '../entity/evento';
import {FormControl, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {UtilsService} from './utils.service';

export class DialogContent {

    type: string;
    title: string;
    subTitle: string;
    okText: string;
    cancelText: string;

    constructor(type: string, title?: string, subTitle?: string, okText?: string, cancelText?: string) {
        this.type = type;
        this.title = title || type === 'delete' ? '¿Seguro que querés eliminar?' : '¿Seguro que querés guardar?';
        this.subTitle = subTitle || type === 'delete' ? 'No podras deshacer esta acción' : 'Al hacerlo, confirmás los cambios';
        this.okText = okText || type === 'delete' ? 'Si, eliminar' : 'Si, guardar';
        this.cancelText = cancelText || 'No, volver';
    }
}

@Injectable()

export class DialogsService {

    actionTriggered = false;

    constructor(private snackBar: MatSnackBar, public dialog: MatDialog, private toastr: ToastrService) {

    }

    openConfirmDialog(data: DialogContent): Promise<boolean | void> {
        const self = this;

        return new Promise<boolean | void>((resolve) => {

            const dialogRef = self.dialog.open(DialogConfirmComponent, {
                width: '400px',
                data: data
            });

            dialogRef.afterClosed().subscribe(result => {
                if (result === 'ok') { resolve(true);
                } else { resolve(false); }
            });

        }).catch( (res) => console.log(res));
    }

    showSnack(message: string, duration?: number, action?: string, position?: any) {
        this.snackBar.open(message, action || 'ok', {
            duration: duration || 5000,
            verticalPosition: position ? position : 'bottom'
        });
    }

    limitExceed(data: any): Promise<boolean | void> {
        const self = this;

        return new Promise<boolean | void>((resolve) => {

            const dialogRef = self.dialog.open(DialogExceedComponent, {
                width: '400px',
                data: data
            });

            dialogRef.afterClosed().subscribe(result => {
                if (result === 'ok') { resolve(true);
                } else { resolve(false); }
            });

        }).catch( (res) => console.log(res));
    }

    showQrRead(): Promise<any | void> {
        const self = this;


        return new Promise<boolean | void>((resolve, reject) => {
            /*
            const dialogRef = self.dialog.open(QrCodeComponent, {
                width: '400px'
            });

            dialogRef.afterClosed().subscribe(result => {
                if (result) {resolve(result);
                } else { reject(null); }
            });
            */

        }).catch( (res) => console.log(res));

    }

    openMail(eventName, dest: string) {
        const self = this;

        return new Promise<boolean | void>((resolve, reject) => {

            const dialogRef = self.dialog.open(MailDialogComponent, {
                width: '400px',
                data: { eventName: eventName, dest: dest }
            });

            dialogRef.afterClosed().subscribe(result => {
                console.log(result);
                if (result) {
                    self.showSuccessToast('', 'Email enviado!');
                    resolve(result);
                } else {
                    self.showErrorToast('Error', 'El mail no ha podido ser enviado, ocurrio un error...')
                    reject(null);
                }
            });

        }).catch( (res) => console.log(res));
    }

    showSuccessToast(title, mssg, time?: number) {
        this.toastr.success(mssg, '', {
            toastClass: 'passwheel-toast-success animated fadeInRight faster',
            timeOut: time || 2000
        });
    }

    showErrorToast(title, mssg, time?: number) {
        this.toastr.error(mssg, '', {
            toastClass: 'passwheel-toast-error animated fadeInRight faster',
            timeOut: time || 2000
        });
    }

    showWarningToast(title, mssg) {
        this.toastr.warning(mssg, title);
    }

    showInfoToast(title, mssg) {
        this.toastr.info(mssg, title);
    }

    openSimpleInputDialog() {
        const self = this;

        return new Promise<boolean | void>((resolve, reject) => {

            const dialogRef = self.dialog.open(SimpleInputComponent, {
                width: '400px'
            });

            dialogRef.afterClosed().subscribe(result => {
                if (result !== null) { resolve(result);
                } else { reject(null); }
            }, error => reject(error));

        }).catch( (res) => console.log(res));
    }

    openSnackBar(message: string, type: string, haveAction?: boolean, actionText?: string, icon?: string, duration?: number) {
        const self = this;

            const tipo = type + '-snackbar';
            let icono = 'info';
            switch(type) {
                case 'info':
                    break;
                case 'danger':
                case 'warning':
                    icono = 'error';
                    break;
                case 'success':
                    icono = 'check_circle';
                    break;
                default:
                    break;
            }
            const snackBarRef = this.snackBar.openFromComponent(SnackBarComponent, {
                duration: duration || 2000,
                horizontalPosition: 'end',
                verticalPosition: 'top',
                panelClass: tipo,
                data: {
                    actionText: actionText || 'Deshacer',
                    haveAction: haveAction || false,
                    message: message,
                    icon: icon || icono
                }
            });
    }
}

@Component({
    selector: 'app-dialog-exceed',
    templateUrl: 'exceed/dialog-exceed.component.html',
    styleUrls: ['exceed/dialog-exceed.component.css']
})
export class DialogExceedComponent {

    constructor(
        public dialogRef: MatDialogRef<DialogExceedComponent>,
        @Inject(MAT_DIALOG_DATA) public data: Evento) {
        this.data = data;
    }

    actionClicked(): void {
        this.dialogRef.close('cancel');
    }

    okClicked(): void {
        this.dialogRef.close('ok');
    }

}

@Component({
    selector: 'app-dialog-confirm',
    templateUrl: 'confirm/dialog-confirm.component.html',
    styleUrls: ['confirm/dialog-confirm.component.css']
})
export class DialogConfirmComponent {

    constructor(
        public dialogRef: MatDialogRef<DialogConfirmComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) {
        this.data = data;
    }

    onNoClick(): void {
        this.dialogRef.close('cancel');
    }

    onOkClick(): void {
        this.dialogRef.close('ok');
    }

}

@Component({
    template: '<div class="d-flex justify-between">' +
        '<div class="d-flex justify-around align-center">' +
        '<mat-icon>{{icon}}</mat-icon> ' +
        '<small>{{message}}</small>' +
        '</div>' +
        '<button *ngIf="haveAction" mat-button class="btn btn-clear " (click)="actionClicked()"><span class="text-white">{{actionText}}</span></button>' +
        '</div>'
})

export class SnackBarComponent {

    message: string;
    actionText: string;
    icon: string;
    haveAction: boolean;

    constructor(@Inject(MAT_SNACK_BAR_DATA) public data: any,
                public snackBar: MatSnackBar) {
        this.icon = data.icon;
        this.message = data.message;
        this.actionText = data.actionText;
        this.haveAction = data.haveAction;
    }

    actionClicked() {
        this.snackBar.dismiss();
    }
}


@Component({
    template: '<div class="p-2">' +
        '<h1 mat-dialog-title>Enviar link</h1>' +
        '<div mat-dialog-content class="animated fadeIn">' +
        '<p>Ingresa el mail al que queres enviar el link</p>' +
        '<mat-form-field class="w-100" appearance="outline">' +
        '<mat-label>Email</mat-label>' +
        '<input matInput  [(ngModel)]="email">' +
        '</mat-form-field>' +
        '</div>' +
        '<div mat-dialog-actions style="margin: 0;justify-content: space-between;min-height: 0;">' +
        '<button mat-button (click)="onNoClick()">Cancelar</button>' +
        '<button mat-button color="primary" (click)="setData()" cdkFocusInitial>Enviar</button>' +
        '</div></div>'
})

export class SimpleInputComponent {

    email: string;

    constructor(public dialogRef: MatDialogRef<DialogConfirmComponent>) {
    }

    onNoClick() {
        this.dialogRef.close(null);
    }

    setData() {
        this.dialogRef.close(this.email);
    }

}


@Component({
    selector: 'app-mail-dialog',
    templateUrl: 'mail-dialog/mail-dialog.component.html',
    styleUrls: ['mail-dialog/mail-dialog.component.css']
})
export class MailDialogComponent {

    emailFormControl = new FormControl('', [
        Validators.required,
        Validators.email,
    ]);

    mssgFormControl = new FormControl('', [
        Validators.required
    ]);

    email: string;
    eventName: string;

    constructor(
        private utils: UtilsService,
        public dialogRef: MatDialogRef<MailDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) {
        this.email = this.data.dest;
        this.eventName = this.data.eventName;
    }

    onNoClick() {
        this.dialogRef.close(false);
    }

    sendMail() {
        this.utils.sendMail(this.eventName, this.emailFormControl.value, this.mssgFormControl.value).subscribe( data => {
            console.log(data);
        }, e => console.log(e));
        this.dialogRef.close(true);
    }

}




