import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import { AngularFireDatabase } from 'angularfire2/database';
import {database} from 'firebase';
import { AngularFirestore } from '@angular/fire/firestore';
import {AuthenticationService} from './authentication.service';
import {UtilsService} from './utils.service';
import {Sessioninfo} from '../entity/sessioninfo';
import {Registry} from '../entity/registry';
import {Evento} from '../entity/evento';
import {DialogsService} from './dialogs.service';

@Injectable()

export class RegistresService {

    public inputs: BehaviorSubject<any[]> = new BehaviorSubject<any[]>(null);
    public data: BehaviorSubject<any[]> = new BehaviorSubject<any[]>(null);
    public registresStats: BehaviorSubject<any> = new BehaviorSubject<any>({
        presents: 234,
        totals: 124,
        news: 67});
    subscribedRegistres: BehaviorSubject<Registry[]> = new BehaviorSubject<Registry[]>([]);

    sessionInfo: Sessioninfo;


    constructor(private utils: UtilsService, private auth: AuthenticationService, private af: AngularFirestore, private aFDB: AngularFireDatabase, private _dialogsService: DialogsService) {
        this.sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
    }

    setRegistresStats(registres: Registry[]) {
        let presents = 0;
        let news = 0;
        registres.forEach( reg => {
            if (reg.present) { presents++; }
            if (reg.isNew) { news++; }
        });
        const aux = {
          presents, totals: registres.length, news
        }
        this.registresStats.next(aux);
    }

    getRegistresStats() {
        return this.registresStats.asObservable();
    }

    getRegistres(idEvent) {
        return database().ref('events/' + idEvent);
        // return this.aFDB.object('events/' + code);
        // return this.aFDB.database.ref('events/' + code );
    }

    getRegistresByIdEvent(idEvent) {
        return database().ref('events/' + idEvent);
    }

    removeRegistry(e: Evento, r: Registry) {
        return database().ref('events/' + e.id + '/' + r.id).remove();
    }

    subscribeRegistres(idEvent) {
        return this.aFDB.object('events/' + idEvent );
    }

    saveRegistry(idEvent, reg) {
        this.aFDB.database.ref('events/' + idEvent + '/' + reg.id).set(reg).then( () => {
            this._dialogsService.showSuccessToast('', 'Registro guardado!');
        }).catch( reason => { this._dialogsService.showErrorToast('Error', 'Ocurrio un problema al guardar los cambios:' + reason ); });
    }

    saveRegistres(ev: Evento, registres: any[]) {
        const regs = this.convertArrayToObject(registres);
        this.aFDB.database.ref('events/' + ev.id ).set(regs).then( (data) => {console.warn(data); });
    }

    addRegistry(reg) {
        // this.aFDB.database.ref('events/' + this.sessionInfo.userCode + '/events/' + this.sessionInfo.eventCode + '/registres/' + reg.id).set(reg);
    }

    saveInputs(eventCode, inputs){
        this.aFDB.database.ref('events/' + this.sessionInfo.userCode + '/events/' + eventCode + '/orderInputs').set(inputs);
    }

    convertObjectArrayToArray(obj: any, createId?: boolean ) {
        createId = false;
        return Object.keys(obj).map(key => {
            const ar = obj[key]
            /*
            if (typeof (ar) == "object" && createId )
              ar.id = key
            */
            return ar;
        });
    }

    convertArrayToObject(regs: any[] ) {
        const objectArray = {};
        regs.forEach( r => {
            objectArray[r.id] = r;
        });
        return objectArray;
    }

    getSubscribedRegistres() {
        return this.subscribedRegistres.asObservable();
    }

}
