import {Injectable} from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import {BehaviorSubject, Subscription} from 'rxjs';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import {AuthenticationService} from './authentication.service';
import {Evento} from '../entity/evento';
import {Sessioninfo} from '../entity/sessioninfo';
import {UserXEvent} from '../entity/user';
import {Chair, Table} from '../entity/table';


@Injectable()

export class TablesService {

    subscribedEvents: BehaviorSubject<Evento[]> = new BehaviorSubject<Evento[]>(null);
    currentEvent: BehaviorSubject<Evento> = new BehaviorSubject<Evento>(null);

    private tablesTable = 'tables';
    private chairsTable = 'chairs';
    private doc: AngularFirestoreDocument<Event>;

    subscriptionList?: Subscription = null;
    public listTables?: Evento[] = [];
    public listChairs?: UserXEvent[] = [];
    sessionInfo: Sessioninfo;

    subsEvent: Subscription;
    subsUserXEvent: Subscription;

    constructor(private auth: AuthenticationService, private af: AngularFirestore, private aFDB: AngularFireDatabase) {
        this.sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
    }


    getTableById(idTable: string) {
        // return this.subscribedUserXEvent.asObservable();
        const self = this;
        return new Promise( (resolve, reject) => {
            self.af.collection<Table>(self.tablesTable, p =>
                p.where('id', '==', idTable)
                    .limit(100)).snapshotChanges().subscribe(res => {
                let eve: Table = null;
                res.map(p => {
                    const d = <Table>p.payload.doc.data();
                    eve = d;
                });
                resolve(eve);
            }, error => reject(null));
        }).catch( err => console.error(err));
    }

    getTablesByEventId(idEvent: string) {
        // return this.subscribedUserXEvent.asObservable();
        const self = this;
        return new Promise( (resolve, reject) => {
            self.af.collection<Table>(self.tablesTable, p =>
                p.where('idEvent', '==', idEvent)
                    .limit(100)).snapshotChanges().subscribe(res => {
                const list: Table[] = [];
                res.map(p => {
                    const d = <Table>p.payload.doc.data();
                    list.push(d);
                });
                resolve(list);
            }, error => reject(null));
        }).catch( err => console.error(err));
    }

    getChairsByTableId(idTable: string) {
        // return this.subscribedUserXEvent.asObservable();
        const self = this;
        return new Promise( (resolve, reject) => {
            self.af.collection<Chair>(self.chairsTable, p =>
                p.where('idTable', '==', idTable)
                    .limit(100)).snapshotChanges().subscribe(res => {
                const list: Chair[] = [];
                res.map(p => {
                    const d = <Chair>p.payload.doc.data();
                    list.push(d);
                });
                resolve(list);
            }, error => reject(null));
        }).catch( err => console.error(err));
    }

    getChairById(idChair: string) {
        // return this.subscribedUserXEvent.asObservable();
        const self = this;
        return new Promise( (resolve, reject) => {
            self.af.collection<Chair>(self.chairsTable, p =>
                p.where('id', '==', idChair)
                    .limit(100)).snapshotChanges().subscribe(res => {
                let e: Chair = null;
                res.map(p => {
                    const d = <Chair>p.payload.doc.data();
                    e = d;
                });
                resolve(e);
            }, error => reject(null));
        }).catch( err => console.error(err));
    }

    saveChair(o: Chair) {
        const b = JSON.parse(JSON.stringify(o));
        this.doc = this.af.doc(`${this.chairsTable}/${o.id}`);
        return this.doc.update(b);
    }

    addChair(o: Chair) {
        const self = this;
        o.id = self.af.createId();
        const b = JSON.parse(JSON.stringify(o));

        return this.af
            .doc(`${this.chairsTable}/${o.id}`)
            .set(b)
            .then(snap => {
                return o.id;
            });
    }

    addTable(o: Table) {
        const self = this;
        o.id = self.af.createId();
        const b = JSON.parse(JSON.stringify(o));

        return this.af
            .doc(`${this.tablesTable}/${o.id}`)
            .set(b)
            .then(snap => {
                return o.id;
            });
    }


    unbind() {
        const self = this;
        if (self.subscriptionList == null) {
            return;
        }
        self.subscriptionList.unsubscribe();
        self.subscriptionList = null;
    }



}
