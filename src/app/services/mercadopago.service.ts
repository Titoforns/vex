import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()

export class MercadopagoService {



    constructor(private http: HttpClient) {

    }

    getAccount() {
        return this.http.get('https://api.mercadopago.com/users/ROB_FORN/mercadopago_account/balance?access_token=APP_USR-3644166766083238-082200-01d8707ca9a94cdbc01f5144f98b2a0e-92104904');
    }

    addPayment() {

        const pay = {
            'token': 'APP_USR-3644166766083238-082200-01d8707ca9a94cdbc01f5144f98b2a0e-92104904',
            'installments': 1,
            'transaction_amount': 58.80,
            'description': 'Point Mini a maquininha que dá o dinheiro de suas vendas na hora',
            'payment_method_id': 'visa',
            'payer': {
                'email': 'titoforns@gmail.com'

            },
            'notification_url': 'https://www.suaurl.com/notificacoes/',
            'sponsor_id': null,
            'binary_mode': false,
            'external_reference': 'MP0001',
            'statement_descriptor': 'MercadoPago',
            'additional_info': {
                'items': [
                    {
                        'id': 'PR0001',
                        'title': 'Point Mini',
                        'picture_url': 'https://http2.mlstatic.com/resources/frontend/statics/growth-sellers-landings/device-mlb-point-i_medium@2x.png',
                        'quantity': 1,
                        'unit_price': 10.80
                    }
                ],
                'payer': {
                    'first_name': 'Nome',
                    'last_name': 'Sobrenome',
                    'address': {
                        'zip_code': '06233-200',
                        'street_name': 'Av das Nacoes Unidas',
                        'street_number': 3003
                    },
                    'registration_date': '2019-01-01T12:01:01.000-03:00',
                    'phone': {
                        'area_code': '011',
                        'number': '987654321'
                    }
                },
                'shipments': {
                    'receiver_address': {
                        'street_name': 'Av das Nacoes Unidas',
                        'street_number': 3003,
                        'zip_code': '06233200'
                    }
                }
            }
        };
        this.http.post('https://api.mercadopago.com/v1/payments?access_token=ACCESS_TOKEN_ENV', pay).subscribe( data => {
            console.log(data);
        }, e => console.warn(e));
    }
}
