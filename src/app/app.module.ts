import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { VexModule } from '../@vex/vex.module';
import { HttpClientModule } from '@angular/common/http';

import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

const firebase = {
  apiKey: 'AIzaSyCszCRVJOzeZZMMN56kOKP2dMASuQB8lSM',
  authDomain: 'passwheel.firebaseapp.com',
  databaseURL: 'https://passwheel.firebaseio.com',
  projectId: 'passwheel',
  storageBucket: 'passwheel.appspot.com',
  messagingSenderId: '32469605046'
};

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,

    // Vex
    VexModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
