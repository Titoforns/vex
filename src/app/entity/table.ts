import {Registry} from './registry';

export class Table {
    width: number;
    height: number;
    position: TablePosition;
    background: string;
    chairsBottom: Chair[];
    chairsTop: Chair[];
    chairsLeft: Chair[];
    chairsRight: Chair[];
    tag: string;
    id: string;
    round: boolean;

    constructor(tag: string, width?: number, height?: number, bg?: string, chairsB?: number, chairsL?: number, chairsT?: number, chairsR?: number) {
        this.tag = tag;
        let chB = null, chT = null, chR = null, chL = null;
        let c = 1;
        if (chairsT) {chT = []; for (let i = 0; i < chairsT; i++) { chT.push(new Chair(c)); c++; } }
        if (chairsL) {chL = []; for (let i = 0; i < chairsL; i++) { chL.push(new Chair(c)); c++; } }
        if (chairsR) {chR = []; for (let i = 0; i < chairsR; i++) { chR.push(new Chair(c)); c++; } }
        if (chairsB) { chB = []; for (let i = 0; i < chairsB; i++) { chB.push(new Chair(c)); c++; } }
        this.width = width || 100;
        this.height = height || 60;
        this.position = new TablePosition();
        this.background = bg || '#fff';
        this.chairsTop = chT || [new Chair(1), new Chair(2)];
        this.chairsLeft = chL || [new Chair(3)];
        this.chairsRight = chR || [new Chair(4)];
        this.chairsBottom = chB || [new Chair(5), new Chair(6)];
        this.round = false;
    }
}

export class RoundTable {
    diameter: number;
    position: TablePosition;
    background: string;
    chairs: Chair[];
    tag: string;
    round: boolean;

    constructor(tag: string, diameter?: number, bg?: string, chairs?: number) {
        let chList: any[] = null;
        if (chairs) {
            chList = [];
            for (let i = 0; i <= chairs; i++) {
                chList.push(new Chair(i + 1));
            }
        }
        this.diameter = diameter || 100;
        this.position = new TablePosition();
        this.background = bg || '#fff';
        this.chairs = chList || [new Chair(1), new Chair(2), new Chair(3), new Chair(4), new Chair(5), new Chair(6) ];
        this.tag = tag;
        this.round = true;
    }
}

export class TablePosition {
    positionX: number;
    positionY: number;

    constructor() {
        this.positionX = 0;
        this.positionY = 0;
    }
}

export class Chair {
    occupied: boolean;
    registry?: Registry;
    id: string;
    idTable: string;
    tag: number;

    constructor(tag: number) {
        this.id = '' + ( new Date().getTime());
        this.occupied = false;
        this.tag = tag;
        this.registry = null;
    }
}