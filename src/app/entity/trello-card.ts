const idEventsList = '5b3e83821ba0826d6e3c8c4a';
const idTrazzoBoard = 'QyjHbsZ7';

export class TrelloCard {

    badges: Badge;
    checkItemStates: any;
    closed: boolean;
    dateLastActivity: string;
    desc: string;
    descData: any;
    due: string;
    dueComplete: boolean;
    dueReminder: number;
    id: string;
    idAttachmentCover: any;
    idBoard: string;
    idChecklists: any[];
    idLabels: any[];
    idList: string;
    idMembers: any[];
    idMembersVoted: any[];
    idShort: number;
    labels: any[];
    manualCoverAttachment: boolean;
    name: string;
    pos: string;
    shortLink: string;
    shortUrl: string;
    subscribed: boolean;
    url: string;

    constructor() {
        this.badges = new Badge();
        this.checkItemStates = null;
        this.closed = false;
        this.dateLastActivity = '';
        this.desc = '';
        this.descData = null;
        this.due = '';
        this.dueComplete = false;
        this.dueReminder = 0;
        this.id = '';
        this.idAttachmentCover = null;
        this.idBoard = idTrazzoBoard;
        this.idChecklists = [];
        this.idLabels = [];
        this.idList = idEventsList;
        this.idMembers = [];
        this.idMembersVoted = [];
        this.idShort = 0;
        this.labels = [];
        this.manualCoverAttachment = false;
        this.name = '';
        this.pos = 'top';
        this.shortLink = '';
        this.shortUrl = '';
        this.subscribed = false;
        this.url = '';
    }
}

export class NewTrelloCard {
    desc: string;
    due: string;
    dueComplete: boolean;
    idLabels: any[];
    idList: string;
    idMembers: any[];
    name: string;
    pos: number;
    urlSource: string;
    fileSource: string;
    idCardSource: string;
    keepFromSource: string;
    address: string;
    locationName: string;
    coordinates: string;

    constructor() {
        this.desc = '';
        this.due = '';
        this.dueComplete = false;
        this.idLabels = [];
        this.idList = idEventsList;
        this.idMembers = [];
        this.name = 'Nueva card de prueba';
        this.pos = 0;
        this.urlSource = '';
        this.fileSource = '';
        this.idCardSource = '';
        this.keepFromSource = '';
        this.address = '';
        this.locationName = '';
        this.coordinates = '';
    }
}

export class Badge {
    attachments: number;
    attachmentsByType: AttachmentByType;
    checkItems: number;
    checkItemsChecked: number;
    comments: number;
    description: boolean;
    due: string;
    dueComplete: boolean;
    fogbugz: string;
    location: boolean;
    subscribed: boolean;
    viewingMemberVoted: boolean;
    votes: number;

    constructor() {
        this.attachments = 0;
        this.attachmentsByType = new AttachmentByType();
        this.checkItems = 0;
        this.checkItemsChecked = 0;
        this.comments = 0;
        this.description = false;
        this.due = '';
        this.dueComplete = false;
        this.fogbugz = '';
        this.location = false;
        this.subscribed = false;
        this.viewingMemberVoted = false;
        this.votes = 0;
    }
}

export class AttachmentByType {
    trello: TrelloType;

    constructor() {
        this.trello = new TrelloType();
    }
}

export class TrelloType {
    board: number;
    card: number;

    constructor() {
        this.board = 0;
        this.card = 0;
    }
}