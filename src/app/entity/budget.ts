export type BudgettStateType = 'ok' | 'standby' | 'sended' | 'refused';

export class Budget {
    name: string;
    dateFrom: firebase.firestore.Timestamp;
    dateTo: firebase.firestore.Timestamp;
    description: string;
    pax: number;
    services: Service[];
    idUser: string;
    state: BudgettStateType;
    total: number;
    place: string;
    id: string;
    accountAssigned: string;

    constructor() {
        this.name = '';
        this.dateFrom = null;
        this.dateTo = null;
        this.description = '';
        this.pax = 0;
        this.services = [];
        this.idUser = '';
        this.state = 'standby';
        this.total = 0;
        this.place = '';
        this.accountAssigned = 'basic';
    }
}

export class Service {
    estimatedPrice: number;
    finalPrice: number;
    name: string;
    qty: number;

    constructor() {
        this.estimatedPrice = 0;
        this.finalPrice = 0;
        this.name = '';
        this.qty = 1;
    }
}

