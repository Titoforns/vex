import {Eventconfig} from './eventconfig';
import {Eventinfo} from './eventinfo';
export type accountType = 'basic' | 'pro' | 'enterprise';
export type invitationType = 'email' | 'qrEmail' | 'preinscription';

export class Evento {
    config: Eventconfig;
    eventInfo: Eventinfo;
    eventAccount: EventAccount;
    invitationConfig: InvitationConfig;
    id: string;
    idUser: string;
    code: string;
    password: string;

    constructor() {
        this.config = new Eventconfig();
        this.eventInfo = new Eventinfo();
        this.eventAccount = new EventAccount('basic');
        this.id = '';
        this.idUser = '';
        this.code = '' + new Date().getTime();
        this.password = '';
    }
}

export class EventAccount {
    accountType: accountType;
    emailEnabled: boolean;
    limitAssistance: number;
    onLineInvitations: boolean;
    ownFields: boolean;
    phoneEnabled: boolean;
    qrEnabled: boolean;
    qrInvitations: boolean;
    tablesEnabled: boolean;
    whatsapp: boolean;
    qrDniEnabled: boolean;
    limitInvitations: number;

    constructor(type: accountType) {
        let limit = 0;
        let limitI = 0;
        switch (type) {
            case 'basic': limit = 50; limitI = 0; break;
            case 'pro': limit = 500; limitI = 1500; break;
            case 'enterprise': limit = 100000; limitI = 10000; break;
            default: limit = 50; limitI = 0; break;
        }
        this.accountType = type;
        this.ownFields = type !== 'basic';
        this.qrEnabled = type !== 'basic';
        this.emailEnabled = type !== 'basic';
        this.whatsapp = type !== 'basic';
        this.phoneEnabled = type !== 'basic';
        this.tablesEnabled = type === 'enterprise';
        this.onLineInvitations = type !== 'basic';
        this.qrInvitations = type === 'enterprise';
        this.qrDniEnabled = type !== 'basic';
        this.limitAssistance = limit;
        this.limitInvitations = limitI;
    }
}

export class InvitationConfig {
    title: string;
    subTitle: string;
    text: string;
    image: string;
    email: string;
    haveQr: boolean;
    qrField: string;
    preinscription: boolean;
    type: invitationType;

    constructor() {
        this.title = '';
        this.subTitle = '';
        this.text = '';
        this.email = '';
        this.haveQr = false;
        this.image = '';
        this.preinscription = false;
        this.qrField = '';
        this.type = 'email';
    }
}