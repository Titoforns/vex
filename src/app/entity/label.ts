export class Label {

  active: boolean;
  height: number;
  width: number;
  lines: ConfigLabel;
  measure: string;
  linesHeight: number;

  constructor() {
      this.active = true;
      this.height = 29;
      this.width = 70;
      this.lines = new ConfigLabel();
      this.measure = 'mm';
      this.linesHeight = 10;
  }
}

export class ConfigLabel {
    input1: LineLabelFirst;
    input2: LineLabel;
    input3: LineLabel;

    constructor() {
        this.input1 = new LineLabelFirst();
        this.input2 = new LineLabel();
        this.input3 = new LineLabel();
    }
}

export class LineLabelFirst {
    ind1: string;
    ind2: string;
    fontSize: number;
    textTransform: string;
    weight: string;
    print: boolean;
    measure: string;
    align: string;

    constructor() {
        this.ind1 = '';
        this.ind2 = '';
        this.fontSize = 3;
        this.textTransform = 'none';
        this.weight = 'normal';
        this.print = true;
        this.measure = 'mm';
        this.align = 'center';
    }
}

export class LineLabel {
    ind1: string;
    fontSize: number;
    textTransform: string;
    weight: string;
    print: boolean;
    measure: string;
    align: string;

    constructor() {
        this.ind1 = '';
        this.fontSize = 3;
        this.textTransform = 'none';
        this.weight = 'normal';
        this.print = true;
        this.measure = 'mm';
        this.align = 'center';
    }
}