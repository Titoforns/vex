export class Sessioninfo {
    userName: string;
    userCode: string;
    name: string;
    lastName: string;
    userPhoto: string;
    type: string;

    constructor() {}
}