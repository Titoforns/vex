export class StockService {
    id: string;
    name: string;
    stock: number;
    state: boolean;
    isThird: boolean;
    cost: number;

    constructor() {}

}