import * as firebase from 'firebase';
import {accountType} from './evento';

export class User {
    name: string;
    lastName: string;
    password: string;
    userName: string;
    type: string;
    id: string;
    userPhoto: string;
    state: boolean;
    account: accountType;

    constructor(account?: accountType) {
        this.name = '';
        this.lastName = '';
        this.password = '';
        this.userName = '';
        this.id = '';
        this.userPhoto = '';
        this.type = 'acreditator';
        this.state = false;
        this.account = account || 'basic';
    }
}

export class Acreditator {
    efficacy: number;
    eventsCount: number;
    idUser: string;
    lastName: string;
    name: string;
    nick: string;
    state: boolean;
    userName: string;
    userPhoto: string;
    id: string;
    type: string;

    constructor() {
        this.efficacy = 0;
        this.eventsCount = 0;
        this.idUser = '';
        this.lastName = '';
        this.name = '';
        this.nick = '';
        this.userName = '';
        this.userPhoto = '';
        this.state = false;
        this.id = '';
        this.type = 'freelance';
    }

}

export class UserXEvent {
    acreditations: number;
    dateEntry: firebase.firestore.Timestamp;
    idAcreditator: string;
    idEvent: string;
    name: string;
    state: boolean;
    userPhoto: string;

    constructor() {
        this.acreditations = 0;
        this.dateEntry = firebase.firestore.Timestamp.fromDate(new Date());
        this.idAcreditator = '';
        this.idEvent = '';
        this.name = '';
        this.state = true;
        this.userPhoto = '';
    }
}

export class QrUser {
    name: string;
    lastName: string;
    dni: string;
    birth: string;
    customField: string;

    constructor() {
        this.name = '';
        this.lastName = '';
        this.dni = '';
        this.birth = '';
        this.customField = '';
    }
}