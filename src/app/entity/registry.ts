import {Orderinput} from './eventconfig';

export class Registry {

    id: number;
    present: boolean;
    hour: number;
    isNew: boolean;
    chairAssigned?: string;

    constructor(orderInputs: Orderinput[]) {
        this.id = new Date().getTime();
        this.hour = new Date().getTime();
        orderInputs.forEach( inp => {
           this[inp.ind] = '';
        });
        this.isNew = true;
    }
}