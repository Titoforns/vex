import {Label} from './label';

export class Eventconfig {
    certify: boolean;
    fileName: string;
    fileState: boolean;
    codeConfig: CodeConfig;
    label: Label;
    orderInputs: Orderinput[];
    account: string;

    constructor() {
        this.certify = false;
        this.fileState = false;
        this.fileName = '';
        this.codeConfig = new CodeConfig();
        this.label = new Label();
        this.orderInputs = [];
    }
}

export class Orderinput {
    certifyOrder: number;
    fieldToFill: boolean;
    ind: string;
    isCode: boolean;
    label: string;
    order: number;
    print: boolean;
    printInCertify: boolean;
    printLine: number;
    required: boolean;
    type: string;
    viewInList: boolean;
    isNew: boolean;
    linkDniField: string;

    constructor(isNew) {
        this.certifyOrder = 99;
        this.fieldToFill = true;
        this.ind = '' + new Date().getTime();
        this.isCode = false;
        this.label = '';
        this.order = 99;
        this.print = false;
        this.printInCertify = false;
        this.printLine = 99;
        this.required = false;
        this.type = 'text';
        this.viewInList = true;
        this.isNew = isNew;
        this.linkDniField = '';
    }
}

export class CodeConfig {
    active: boolean;
    fieldToSearch: string;
    autoPresent: boolean;
    autoPrint: boolean;
    autoReset: boolean;
    codeFromDni: boolean;

    constructor() {
        this.active = true;
        this.fieldToSearch = 'dni';
        this.autoPresent = false;
        this.autoPrint = false;
        this.autoReset = false;
        this.codeFromDni = false;
    }
}