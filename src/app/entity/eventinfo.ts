import * as firebase from 'firebase';

export class Eventinfo {

  backgroundImage: string;
  name: string;
  code: string;
  dateFrom: firebase.firestore.Timestamp;
  dateTo: firebase.firestore.Timestamp;
  description: string;
  password: string;
  state: boolean;
  location: string;
  presents: number;
  totalRegistres: number;
  news: number;
  link?: string;

  constructor() {
    this.backgroundImage = '';
    this.name = '';
    this.code = '';
    this.dateFrom = firebase.firestore.Timestamp.fromDate(new Date());
    this.dateTo = firebase.firestore.Timestamp.fromDate(new Date());
    this.description = '';
    this.password = '';
    this.state = false;
    this.location = '';
    this.presents = 0;
    this.totalRegistres = 0;
    this.news = 0;
  }
}
